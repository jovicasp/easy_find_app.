-- MySQL dump 10.13  Distrib 5.7.30, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: laravel_easy_find
-- ------------------------------------------------------
-- Server version	5.7.22-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `UUID` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_category` bigint(20) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `categories_category_id_foreign` (`parent_category`),
  CONSTRAINT `categories_category_id_foreign` FOREIGN KEY (`parent_category`) REFERENCES `categories` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'E7CduGaAKPU8nua','Furniture',NULL,NULL,NULL),(2,'XD3Zb2CNYCZ5yf4','Living_room',1,NULL,NULL),(3,'Ib7BjcI7bJGmS6p','Dining_room',1,NULL,NULL),(4,'I4IHPzEgHMgFE03','Sleeping_room',1,NULL,NULL),(5,'VTW3ATPlY7iwYNg','Kitchen',1,NULL,NULL),(6,'9DE5jNSkCWtRJkf','Living room Tables',2,NULL,'2020-04-04 18:26:32'),(7,'i4zzj9xkTgLqNRm','Living_room_chairs',2,NULL,NULL),(8,'l0RGhR4x4ljixUT','Shelf_with_drawers',2,NULL,NULL),(9,'2S33YAVDsPKXVT3','Sofas',2,NULL,NULL),(10,'Ono8NaJ3sLHONYS','TV_shelfs',2,NULL,NULL),(11,'OOiUBnWKabzkT54','table_chairs',7,NULL,NULL),(12,'TAzF3CFiHMcAomb','bar_chairs',7,NULL,NULL),(13,'O4jRy7hanXtosPd','dining_tables',3,NULL,NULL),(14,'69QCDqg8LkdEXoK','dining_chairs',3,NULL,NULL),(15,'5WfQ4LISO2v9Fep','beds',4,NULL,NULL),(16,'RWpNExWEb3WkGCY','kitchen_tables',5,NULL,NULL),(17,'NQVFAca7BnS4F2S','kitchen_shelfs',5,NULL,NULL),(18,'BI97BwokIp7Tji1','Home_Appliances',NULL,NULL,NULL),(19,'SYnhs7WGguv5rlB','Microwaves',18,NULL,NULL),(20,'oyyRTwX0YQjBTKQ','Mixers',18,NULL,NULL),(21,'UU6EW6KqRvJeGpm','Lamps',18,NULL,NULL),(22,'grye5KxaA9wCabd','table_lamps',21,NULL,NULL),(23,'PulsFlDMxBYRZm5','wall_lamps',21,NULL,NULL),(24,'f8tLgzfsy96epc2','standing_lamps',21,NULL,NULL),(25,'PJXUTdymHf5utAb','living_room_Standing_lamps',24,NULL,NULL),(26,'nobd3BVp6MGBnFp','sleeping_room_Sls',24,NULL,NULL),(27,'1Bze45XxLzDbn96','Gadgets',NULL,NULL,NULL),(28,'96dddcd4-8fe7-48d2-9c7a-d64f8d9d5731','IT devices',NULL,'2019-12-27 20:47:48','2019-12-27 20:47:48'),(30,'920e00a1-2b20-423d-b98f-e7c622b45414','laptopsXXX',28,'2019-12-27 22:37:35','2019-12-27 22:50:09'),(32,'d8a0c51d-14b1-41fd-bfe4-fb6e71a068db','Big tables',6,'2020-04-04 18:28:32','2020-04-04 18:28:32'),(34,'50e807be-544d-4bc1-9e58-65c4d78f3f07','Nova ZERO categorija',34,'2020-04-11 12:09:14','2020-04-11 13:28:58'),(35,'ac8025c5-f72b-4cd4-9217-cc718bcfaef9','Podkategorija od NOVE ZERO KATEGORIJE',34,'2020-04-11 12:10:05','2020-04-11 12:10:05'),(41,'322cdcb5-d551-45f8-af9b-b658ebd96584','Books',NULL,'2020-04-30 22:24:27','2020-04-30 22:24:27'),(53,'8731c380-579c-4cb0-b84e-2402043c8ebf','History',41,'2020-05-03 22:06:46','2020-05-03 22:06:46'),(54,'1db8f21e-fe83-441c-aadc-107eaa61c08f','Poetry',41,'2020-05-03 22:07:06','2020-05-03 22:07:06'),(55,'35ad54bf-ced0-4a3e-99d7-d5676f1329b9','Carpets',NULL,'2020-05-05 15:52:47','2020-05-05 15:52:47'),(58,'ecee2d5c-76dc-4c2c-ae9a-2eb1ca586535','Carpets222',NULL,'2020-05-05 15:54:19','2020-05-05 15:54:19');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `items`
--

DROP TABLE IF EXISTS `items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `items` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `UUID` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` bigint(20) unsigned DEFAULT NULL,
  `picture` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `items_category_id_foreign` (`category_id`),
  CONSTRAINT `items_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=127 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `items`
--

LOCK TABLES `items` WRITE;
/*!40000 ALTER TABLE `items` DISABLE KEYS */;
INSERT INTO `items` VALUES (1,'zwFSlBzTXLEnmNP','Table1',6,'table.jpg','SHORT DESCRIPTION!!                                            Laravel is a feature-rich framework. However, you cannot find all the available features in the official documentation. Here are some features that you may not know.',NULL,'2020-04-03 15:44:47'),(2,'IuVDU7cc5abjHHt','Table2',6,'table2.jpg','Laravel is a feature-rich framework. However, you cannot find all the available features in the official documentation. Here are some features that you may not know.',NULL,NULL),(3,'cAtovaLpIfVLpS4','Table3',6,'table3.jpg','Laravel is a feature-rich framework. However, you cannot find all the available features in the official documentation. Here are some features that you may not know.',NULL,NULL),(4,'917ZFpOYXo2ZnIe','TableChair1',11,'table_chair1.jpg','Laravel is a feature-rich framework. However, you cannot find all the available features in the official documentation. Here are some features that you may not know.',NULL,NULL),(5,'fWiIpgzLHfHYNev','TableChair2',11,'table_chair2.jpg','Laravel is a feature-rich framework. However, you cannot find all the available features in the official documentation. Here are some features that you may not know.',NULL,NULL),(6,'cyuzd6bnZDcpFFu','TableChair3',11,'table_chair3.jpg','Laravel is a feature-rich framework. However, you cannot find all the available features in the official documentation. Here are some features that you may not know.',NULL,NULL),(7,'5J73XMV0KqbTGf2','TableChair4',11,'dummy_item.png','Laravel is a feature-rich framework. However, you cannot find all the available features in the official documentation. Here are some features that you may not know.',NULL,NULL),(8,'aodJo1UupSfeVR6','BarChair1',12,'bar_chair1.jpg','Laravel is a feature-rich framework. However, you cannot find all the available features in the official documentation. Here are some features that you may not know.',NULL,NULL),(9,'fOEQHfIbTnxHL8X','BarChair2',12,'bar_chair2.jpg','Laravel is a feature-rich framework. However, you cannot find all the available features in the official documentation. Here are some features that you may not know.',NULL,NULL),(10,'qiIzmJdMCoouIJk','Shelf1',8,'shelf.jpeg','Laravel is a feature-rich framework. However, you cannot find all the available features in the official documentation. Here are some features that you may not know.',NULL,NULL),(11,'nEAVJNhhEu1yukH','Shelf2',8,'dummy_item.png','Laravel is a feature-rich framework. However, you cannot find all the available features in the official documentation. Here are some features that you may not know.',NULL,NULL),(12,'vutvrhB8SqXDDIb','Shelf3',8,'dummy_item.png','Laravel is a feature-rich framework. However, you cannot find all the available features in the official documentation. Here are some features that you may not know.',NULL,NULL),(13,'0wTxZoRSUnsYmTi','Sofa1',9,'sofa.jpg','Laravel is a feature-rich framework. However, you cannot find all the available features in the official documentation. Here are some features that you may not know.',NULL,NULL),(14,'dI1kZFM0FoIqq6i','Sofa2',9,'dummy_item.png','Laravel is a feature-rich framework. However, you cannot find all the available features in the official documentation. Here are some features that you may not know.',NULL,NULL),(15,'QmWkRNBVBoy5zPR','Sofa3',9,'dummy_item.png','Laravel is a feature-rich framework. However, you cannot find all the available features in the official documentation. Here are some features that you may not know.',NULL,NULL),(16,'xazF6Gbt2X8BG6M','Sofa4',9,'dummy_item.png','Laravel is a feature-rich framework. However, you cannot find all the available features in the official documentation. Here are some features that you may not know.',NULL,NULL),(17,'bJScQlsO4YcYDM1','TV_Shelf1',10,'TV_Shelf1.jpg','Laravel is a feature-rich framework. However, you cannot find all the available features in the official documentation. Here are some features that you may not know.',NULL,NULL),(18,'uWJKJncnuKw5jLb','TV_Shelf2',10,'dummy_item.png','Laravel is a feature-rich framework. However, you cannot find all the available features in the official documentation. Here are some features that you may not know.',NULL,NULL),(19,'ZzlKFnUkagywrsu','TV_Shelf3',10,'dummy_item.png','Laravel is a feature-rich framework. However, you cannot find all the available features in the official documentation. Here are some features that you may not know.',NULL,NULL),(20,'ELWwjZ1uLjrGQys','Dining_Table1',13,'Dining_Table1.jpg','Laravel is a feature-rich framework. However, you cannot find all the available features in the official documentation. Here are some features that you may not know.',NULL,NULL),(21,'YQ2nIZuZbou05eT','Dining_Table2',13,'dummy_item.png','Laravel is a feature-rich framework. However, you cannot find all the available features in the official documentation. Here are some features that you may not know.',NULL,NULL),(22,'CADX6dRJ3BPAh6U','Dining_Table3',13,'dummy_item.png','Laravel is a feature-rich framework. However, you cannot find all the available features in the official documentation. Here are some features that you may not know.',NULL,NULL),(23,'pQYG9LA6TmvzbB6','Dining_Chair1',14,'Dining_Chair1.jpeg','Laravel is a feature-rich framework. However, you cannot find all the available features in the official documentation. Here are some features that you may not know.',NULL,NULL),(24,'0ptiPnvgqtjxsVZ','Dining_Chair2',14,'dummy_item.png','Laravel is a feature-rich framework. However, you cannot find all the available features in the official documentation. Here are some features that you may not know.',NULL,NULL),(25,'DOnXZSGKieR8Ezk','Dining_Chair3',14,'dummy_item.png','Laravel is a feature-rich framework. However, you cannot find all the available features in the official documentation. Here are some features that you may not know.',NULL,NULL),(26,'nDu1yfWki4IF3QJ','Dining_Chair4',14,'dummy_item.png','Laravel is a feature-rich framework. However, you cannot find all the available features in the official documentation. Here are some features that you may not know.',NULL,NULL),(27,'wtOCPdJKGf2hiaN','Dining_Chair5',14,'dummy_item.png','Laravel is a feature-rich framework. However, you cannot find all the available features in the official documentation. Here are some features that you may not know.',NULL,NULL),(28,'o7bollDtprZ9Gzn','Dining_Chair6',14,'dummy_item.png','Laravel is a feature-rich framework. However, you cannot find all the available features in the official documentation. Here are some features that you may not know.',NULL,NULL),(29,'Yr69qF7x9KZNwzq','Bed1',15,'bad_20200220004854.jpg','Laravel is a feature-rich framework. However, you cannot find all the available features in the official documentation. Here are some features that you may not know.',NULL,'2020-02-20 00:48:54'),(30,'8HqPBzwLuLMmwui','Bed2',15,'dummy_item.png','Laravel is a feature-rich framework. However, you cannot find all the available features in the official documentation. Here are some features that you may not know.',NULL,NULL),(31,'0szbGBhvlxGVp8T','Bed3',15,'dummy_item.png','Laravel is a feature-rich framework. However, you cannot find all the available features in the official documentation. Here are some features that you may not know.',NULL,NULL),(32,'6GVycfJiGvrRP7m','Bed4',15,'dummy_item.png','Laravel is a feature-rich framework. However, you cannot find all the available features in the official documentation. Here are some features that you may not know.',NULL,NULL),(33,'yO6gfbcXw4FZpMZ','Kitchen_table1',16,'dummy_item.png','Laravel is a feature-rich framework. However, you cannot find all the available features in the official documentation. Here are some features that you may not know.',NULL,NULL),(34,'dGBDSnhP3syY2LT','Kitchen_table2',16,'dummy_item.png','Laravel is a feature-rich framework. However, you cannot find all the available features in the official documentation. Here are some features that you may not know.',NULL,NULL),(35,'8lpJSlDhXI6xwYr','Kitchen_shelf1',17,'dummy_item.png','Laravel is a feature-rich framework. However, you cannot find all the available features in the official documentation. Here are some features that you may not know.',NULL,NULL),(36,'5ncOUgEpGgVCxdj','Kitchen_shelf2',17,'dummy_item.png','Laravel is a feature-rich framework. However, you cannot find all the available features in the official documentation. Here are some features that you may not know.',NULL,NULL),(37,'33PS0hd1waCJR0Y','MW1',19,'MW1_20200220004903.jpg','Laravel is a feature-rich framework. However, you cannot find all the available features in the official documentation. Here are some features that you may not know.',NULL,'2020-02-20 00:49:03'),(38,'fW130yh3njjWdVO','MW2',19,'dummy_item.png','Laravel is a feature-rich framework. However, you cannot find all the available features in the official documentation. Here are some features that you may not know.',NULL,NULL),(39,'YFZOVW9EJWMC978','MW3',19,'dummy_item.png','Laravel is a feature-rich framework. However, you cannot find all the available features in the official documentation. Here are some features that you may not know.',NULL,NULL),(40,'UZr2MWN1koJBMV0','MW4',19,'dummy_item.png','Laravel is a feature-rich framework. However, you cannot find all the available features in the official documentation. Here are some features that you may not know.',NULL,NULL),(41,'9Qo4JLNLmM7a5sS','Mixer1',20,'mixer_20200220004910.jpeg','Laravel is a feature-rich framework. However, you cannot find all the available features in the official documentation. Here are some features that you may not know.',NULL,'2020-02-20 00:49:10'),(42,'aKPpwsdMZmi9pYY','Mixer2',20,'dummy_item.png','Laravel is a feature-rich framework. However, you cannot find all the available features in the official documentation. Here are some features that you may not know.',NULL,NULL),(43,'ZFqf8UHYtug8qYL','TL1',22,'dummy_item.png','Laravel is a feature-rich framework. However, you cannot find all the available features in the official documentation. Here are some features that you may not know.',NULL,NULL),(44,'PUNtXkC6FeeXTTo','TL2',22,'dummy_item.png','Laravel is a feature-rich framework. However, you cannot find all the available features in the official documentation. Here are some features that you may not know.',NULL,NULL),(45,'nwq5pNFBSZFyONQ','TL3',22,'dummy_item.png','Laravel is a feature-rich framework. However, you cannot find all the available features in the official documentation. Here are some features that you may not know.',NULL,NULL),(46,'2dmL3FRhVOtYdb1','TL4',22,'dummy_item.png','Laravel is a feature-rich framework. However, you cannot find all the available features in the official documentation. Here are some features that you may not know.',NULL,NULL),(47,'RIemarNvcblbl2n','TL5',22,'dummy_item.png','Laravel is a feature-rich framework. However, you cannot find all the available features in the official documentation. Here are some features that you may not know.',NULL,NULL),(48,'JPos3j0ZTQpIbwK','WL1',23,'dummy_item.png','Laravel is a feature-rich framework. However, you cannot find all the available features in the official documentation. Here are some features that you may not know.',NULL,NULL),(49,'cPkxaTAKLRbjBbp','WL2',23,'dummy_item.png','Laravel is a feature-rich framework. However, you cannot find all the available features in the official documentation. Here are some features that you may not know.',NULL,NULL),(50,'ynVGsmszZqWarHZ','WL3',23,'dummy_item.png','Laravel is a feature-rich framework. However, you cannot find all the available features in the official documentation. Here are some features that you may not know.',NULL,NULL),(51,'3wBDVUrK7AwZ5UJ','WL4',23,'dummy_item.png','Laravel is a feature-rich framework. However, you cannot find all the available features in the official documentation. Here are some features that you may not know.',NULL,NULL),(52,'oY0Ssb5wUhKgUWq','LRSL1',25,'dummy_item.png','Laravel is a feature-rich framework. However, you cannot find all the available features in the official documentation. Here are some features that you may not know.',NULL,NULL),(53,'2omaemfTU24kjuv','LRSL2',25,'dummy_item.png','Laravel is a feature-rich framework. However, you cannot find all the available features in the official documentation. Here are some features that you may not know.',NULL,NULL),(54,'rAQI5Rf4NdXFksq','LRSL3',25,'dummy_item.png','Laravel is a feature-rich framework. However, you cannot find all the available features in the official documentation. Here are some features that you may not know.',NULL,NULL),(55,'FiSwAv6WE4KD712','SRSL1',26,'dummy_item.png','Laravel is a feature-rich framework. However, you cannot find all the available features in the official documentation. Here are some features that you may not know.',NULL,NULL),(56,'FiSwAv6xxxKD734','SRSL2',26,'dummy_item.png','Laravel is a feature-rich framework. However, you cannot find all the available features in the official documentation. Here are some features that you may not know.',NULL,NULL),(57,'FiSwAv6yyyKD756','gadget1',27,'dummy_item.png','Laravel is a feature-rich framework. However, you cannot find all the available features in the official documentation. Here are some features that you may not know.',NULL,NULL),(58,'FiSwAv6ZZZKD778','gadget2',27,'dummy_item.png','Laravel is a feature-rich framework. However, you cannot find all the available features in the official documentation. Here are some features that you may not know.',NULL,NULL),(59,'77f10a26-a4dc-4c11-95e5-a1fb2a9de43f','FURNITURE_zero_item1',1,'dummy_item_20191222091302.png','Root item in zero category.','2019-12-22 09:13:02','2019-12-22 09:13:02'),(60,'6f856162-27b4-4d55-a6ad-1449324c52b6','Living_room_subcategory_item0',2,'dummy_item_20191222091705.png','Subcategory item.','2019-12-22 09:17:05','2019-12-22 09:17:05'),(63,'5b2de672-7298-435d-90cc-05b83c33c0b1','laptop2',28,'LT1_20200220004924.jpeg','dsgtdsg  h reh e','2019-12-27 22:37:04','2020-02-20 00:49:24'),(68,'2f331351-807c-472b-961e-958308127ff4','1',32,NULL,'1','2020-04-04 19:01:22','2020-04-04 19:01:22'),(69,'90e7878f-8516-4ae9-898c-e48569eed6fa','2',32,NULL,'2','2020-04-04 19:01:36','2020-04-04 19:01:36'),(96,'964377d6-45a1-4601-8746-d4bd5d51e8d7','NOVO IME',32,'filter maska_20200410015541.png','aaaa','2020-04-08 10:29:12','2020-04-10 01:55:41'),(100,'61331e1f-b2ad-488a-93a5-ed8993674f82','rrrrrrrrrr',1,'Kostunica_20200411130521.jpg','ssss','2020-04-11 13:05:22','2020-04-11 13:05:39'),(102,'7a18061f-fd30-45c4-9c2c-d9142ef1c93e','Table1',6,'banica zamirisuje_20200411134713.','qqqqqqqqqq','2020-04-11 13:47:13','2020-04-11 13:47:13'),(115,'3548af27-003d-49c4-a184-822308ab945d','History book1',53,'bookH1_1588543790.png','1111111111','2020-05-03 22:09:50','2020-05-03 22:09:50'),(116,'12f18436-907f-4b80-840d-ecb5b80c6efa','History book2',53,'BookH2_1588544032.jpeg','222222222222','2020-05-03 22:13:52','2020-05-03 22:13:52'),(117,'29f2a923-a608-47ee-9606-a959e9599efe','History book3',53,'BookH3_1588544075.jpg','333333333333','2020-05-03 22:14:35','2020-05-03 22:14:35'),(118,'cc4c7ce0-76ed-4beb-aecd-5cb5e4b0eb54','Poetry book1',54,'BookPoetry1_1588544097.png','111111111111','2020-05-03 22:14:57','2020-05-03 22:14:57'),(119,'2f73aa6b-fc1a-4b8b-84e6-2332cac38da6','Poetry book2',54,'BookPoetry2_1588544120.jpg','2222222222','2020-05-03 22:15:20','2020-05-03 22:15:20');
/*!40000 ALTER TABLE `items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (2,'2014_10_12_100000_create_password_resets_table',1),(3,'2019_08_19_000000_create_failed_jobs_table',1),(4,'2019_11_19_141341_create_categories_table',1),(5,'2019_11_22_135306_create_items_table',1),(7,'2014_10_12_000000_create_users_table',2);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `role` tinyint(4) NOT NULL DEFAULT '2',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,1,'admin','admin@mail.com',NULL,'$2y$10$BgXewEOZorqLLBnSVKaN/OO3KGRxhYMX4ez9LsM24JPt5LGW8Q5be',NULL,'2019-12-22 20:41:55','2019-12-22 20:41:55'),(2,2,'user','user@mail.com',NULL,'$2y$10$gAXTC2J6XnfMkUU3z9l3guVpQsRNwQJMr4enu/NBna3gd3kqAmGl.',NULL,'2019-12-22 22:56:38','2019-12-22 22:56:38'),(3,2,'user1','user1@mail.com',NULL,'$2y$10$AqCikVNG/VzqkdxUaUZxnOeA/K8twcLeSjxIkmsjjLliZ3EoVNWZy',NULL,'2019-12-27 21:28:09','2019-12-27 21:28:09'),(4,2,'user2','user2@mail.com',NULL,'$2y$10$x2Q2J3lM01W4Yh3SYSPYm.cUDo4Q4TRSI1eXf7IWcvmXehVs/jjAS',NULL,'2020-04-04 17:14:43','2020-04-04 17:14:43');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-05-05 20:50:45
