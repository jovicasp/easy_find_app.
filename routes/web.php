<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome0');
});
Auth::routes();

/**
 * Admin routes
 * */
Route::middleware('admin')->group(function () {
//SEARCH
    Route::get('/search', 'CategoryController@getSearch')->name('categories.search');
//CATEGORIES
    Route::get('/categories', 'CategoryController@index')->name('categories.index');
    Route::get('/categories/create', 'CategoryController@create')->name('categories.create');
    Route::post('/categories', 'CategoryController@store')->name('categories.store');
    Route::get('/categories/{category}', 'CategoryController@show')->name('categories.show');
    Route::get('/categories/{category}/edit', 'CategoryController@edit')->name('categories.edit');
    Route::put('/categories/{category}', 'CategoryController@update')->name('categories.update');
    Route::delete('/categories/{category}', 'CategoryController@destroy')->name('categories.destroy');
//ITEMS
    Route::get('/items', 'ItemController@index')->name('items.index');
    Route::get('/items/create', 'ItemController@create')->name('items.create');
    Route::post('/items', 'ItemController@store')->name('items.store');
    Route::get('/items/{item}', 'ItemController@show')->name('items.show');
    Route::get('/items/{item}/edit', 'ItemController@edit')->name('items.edit');
    Route::put('/items/{item}', 'ItemController@update')->name('items.update');
    Route::delete('/items/{item}', 'ItemController@destroy')->name('items.destroy');

});

/**
 *  All logged in users
 * */
Route::middleware('auth')->group(function () {
//SEARCH
    Route::get('/search', 'CategoryController@getSearch')->name('categories.search');
    Route::get('/categories/{category}', 'CategoryController@show')->name('categories.show');
    Route::get('/items/{item}', 'ItemController@show')->name('items.show');

});