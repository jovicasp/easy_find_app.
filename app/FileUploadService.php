<?php


namespace App;


use Illuminate\Support\Facades\Request;

class FileUploadService
{
    protected $name;

    public function upload($name)
    {
        $file = Request::file($name);
        $fileNameWithoutExt = preg_replace('/\\.[^.\\s]{3,4}$/', '', $file->getClientOriginalName());
        $fileName = $fileNameWithoutExt . '_' . date_timestamp_get(now()) . '.' . $file->getClientOriginalExtension();
        $file->storeAs('public/images', $fileName);
        return $fileName;
    }
}