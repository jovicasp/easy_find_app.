<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;

class Item extends Model
{
    protected $fillable = ['name', 'category_id', 'picture', 'description'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function categories()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    /**
     * Path to the specific resource
     * @return string
     */
    public function path()
    {
        return route('items.show', $this);
    }

    /**
     *  Setup model event hooks - UUID
     *  Generating a UUID for every instance
     */
    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->uuid = (string)Uuid::generate(4);
        });
    }
}