<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;
use Webpatser\Uuid\Uuid;

class Category extends Model
{
    protected $fillable = ['name', 'parent_category'];

    /**
     * Subcategories
     * @return HasMany
     */
    public function categories()
    {
        return $this->hasMany(Category::class, 'parent_category');
    }

    /**
     * Recursive relationship
     * @return HasMany
     */
    public function childrenCategories()
    {
        return $this->hasMany(Category::class, 'parent_category')->with("categories");
    }

    /**
     * Parent Category
     * @return BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * Recursive method - gets all subcategories of all levels. It loops over the collection of subcategories (categories) ,
     * adding current subcategory to the collection. Then, the method calls itself, on that subcategory.
     * @return Collection
     */
    public function getAllChildrenCategories ()
    {
        $collection = new Collection();

        foreach ($this->categories as $subcategory) {
            $collection->push($subcategory);
            $collection = $collection->merge($subcategory->getAllChildrenCategories());
        }
        return $collection;
    }

    /**
     * Items
     * @return HasMany
     */
    public function items()
    {
        return $this->hasMany(Item::class);
    }

    /**
     * Path to the specific resource
     * @return string
     */
    public function path()
    {
        return route('categories.show', $this);
    }

    /**
     *  Setup model event hooks - UUID
     *  Generating a UUID for every instance
     */
    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->uuid = (string)Uuid::generate(4);
        });
    }
}