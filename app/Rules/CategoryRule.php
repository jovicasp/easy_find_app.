<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class CategoryRule implements Rule
{
    /**
     * @var array $subCategoriesNames
     */
    private $subCategoriesNames;

    /**
     * Create a new rule instance.
     *
     * @param array $subCategoriesNames
     */
    public function __construct(array $subCategoriesNames)
    {
        $this->subCategoriesNames = $subCategoriesNames;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return !(in_array(strtolower($value),  array_map("strtolower", $this->subCategoriesNames)));
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Choose another category name. Reasons:
        Chosen parent category already contains subcategory with the same name.
        Category name and parent category name are the same.';
    }
}
