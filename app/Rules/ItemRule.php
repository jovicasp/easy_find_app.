<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class ItemRule implements Rule
{
    /**
     * @var array $categoryItemNames
     */
    private $categoryItemNames;

    /**
     * Create a new rule instance.
     *
     * @param array $categoryItemNames
     */
    public function __construct(array $categoryItemNames)
    {
        $this->categoryItemNames = $categoryItemNames;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  array  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return !(in_array(strtolower($value),  array_map('strtolower', $this->categoryItemNames)));
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Choose another name. Chosen category already contains item with the same name.';
    }
}
