<?php

namespace App\Http\Controllers;

use App\Category;
use App\FileUploadService;
use App\Item;
use App\Rules\ItemRule;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;

class ItemController extends Controller
{
    protected $upload;

    /**
     * ItemController constructor.
     * @param FileUploadService $upload
     */
    public function __construct(FileUploadService $upload)
    {
        $this->upload = $upload;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $items = Item::all();

        return view('items.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     * @throws \Exception
     */
    public function create()
    {
        $categories = Category::all();

        return view('items.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validateItem($this->getCategoryItemNames($request));
        //FILE UPLOAD
        $fileName = null;
        if ($request->file('picture')) {
            $fileName = $this->upload->upload('picture');
        }

        $data = [
            'name' => $request->input('item_name'),
            'category_id' => $request->get('categories'),
            'picture' => $fileName,
            'description' => $request->description
        ];

        Item::create($data);
        return redirect(route('items.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param Item $item
     * @return Response
     */
    public function show(Item $item)
    {
        return view('items.show', [
            'item' => $item
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Item $item
     * @return Response
     */
    public function edit(Item $item)
    {
        $categories = Category::all();

        return view('items.edit', compact('item', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Item $item
     * @return Response
     */
    public function update(Request $request, Item $item)
    {
        $categoryItemNames = $this->getCategoryItemNames($request);
        unset($categoryItemNames[array_search($item->name, $categoryItemNames)]);
        $this->validateItem($categoryItemNames);
        //FILE UPLOAD
        $fileName = $item->picture;
        if ($request->file('picture')) {
            $fileName = $this->upload->upload('picture');
        }

        $data = [
            'name' => $request->input('item_name'),
            'category_id' => $request->get('categories'),
            'picture' => $fileName,
            'description' => $request->description
        ];

        $item->update($data);
        return redirect($item->path());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Item $item
     * @return Response
     * @throws \Exception
     */
    public function destroy(Item $item)
    {
        Storage::disk('public')->delete('images/' . $item->picture);

        $item->delete();
        return redirect(route('items.index'));
    }

    /**
     * TODO: Validation: do it here or better in Request files
     * @param $param
     * @return array
     */
    public function validateItem($param): array
    {
        return request()->validate([
            'item_name' => ['bail', 'required', 'string', 'max:255', new ItemRule($param) ],
            'categories' => 'bail|required|filled|exists:categories,id',
            'description' => 'bail|required|string|max:1000',
            'picture' => 'bail|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);
    }

    /**
     * @param Request $request
     * @return  array
     */
    public function getCategoryItemNames(Request $request)
    {
        $categoryItemNames = [];
        $category = Category::find($request->get('categories'));
        if (!($category === null)){
            $categoryItemNames = $category->items->pluck('name')->toArray();
        }

        return $categoryItemNames;
    }
}
