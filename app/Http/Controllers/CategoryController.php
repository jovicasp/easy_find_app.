<?php

namespace App\Http\Controllers;

use App\Category;
use App\Item;
use App\Rules\CategoryRule;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\View\View;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $categories = Category::whereNull('parent_category')
            ->with('childrenCategories')
            ->get();

        return view('categories.index', compact('categories'));
    }

    /**
     * Display a results of the search.
     *
     * @param Request $request
     * @return Factory|View
     */
    public function getSearch(Request $request)
    {
        $keyword = $request->get('input');

        $items = Item::where('name', 'LIKE', '%' . $keyword . '%')->with('categories')->get();
        $zerocategories = Category::whereNull('parent_category')->where('name', 'LIKE', '%' . $keyword . '%')->get();
        $subcategories = Category::whereNotNull('parent_category')->where('name', 'LIKE', '%' . $keyword . '%')->get();

        return view('categories.results', compact('zerocategories', 'subcategories', 'items'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $categories = Category::all();

        return view('categories.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validateCategory($this->getSubCategoriesNames($request));

        $data = [
            'name' => $request->input('category-name'),
            'parent_category' => $request->get('parent-category')
        ];

        Category::create($data);
        return redirect(route('categories.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param Category $category
     * @return Response
     */
    public function show(Category $category)
    {
        return view('categories.show', ['category' => $category]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Category $category
     * @return Response
     */
    public function edit(Category $category)
    {
        $categories = Category::all();
        //Send collection of categories (for array of parent categories) without category that is edited.
        $categories->pull($categories->whereIn('name', $category->name)->keys()[0]);

        return view('categories.edit', compact('category', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Category $category
     * @return Response
     */
    public function update(Request $request, Category $category)
    {
        $subCategoriesNames = $this->getSubCategoriesNames($request);
        if ($currentSubCategoryKey = array_search($category->name, $subCategoriesNames)) {
            unset($subCategoriesNames[$currentSubCategoryKey]);
        }
        $this->validateCategory($subCategoriesNames);

        $data = [
            'name' => $request->input('category-name'),
            'parent_category' => $request->get('parent-category'),
        ];

        $category->update($data);
        return redirect($category->path());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Category $category
     * @return Response
     * @throws \Exception
     */
    public function destroy(Category $category)
    {
        $category->delete();

        return redirect(route('categories.index'));
    }

    /**
     * TODO: Validation: do it here or better in Request files
     * @param $param
     * @return array
     */
    public function validateCategory($param): array
    {
        return request()->validate([
            'category-name' => ['bail', 'required', 'string', 'max:255', new CategoryRule($param)],
            'parent-category' => 'nullable|exists:categories,id'
        ]);
    }

    /**
     * Get array of subcategories names
     * @param Request $request
     * @return array
     */
    public function getSubCategoriesNames(Request $request)
    {
        if ($request->get('parent-category') === null) {
            $subCategoriesNames = Category::all()
                ->pluck('name')
                ->toArray();
        } else {
            $subCategoriesNames = Category::where('id', $request->get('parent-category'))
                ->firstOrFail()
                ->getAllChildrenCategories()
                ->pluck('name')
                ->toArray();
            $parentCategoryName = Category::where('id', $request->get('parent-category'))->firstOrFail()->name;
            array_push($subCategoriesNames, $parentCategoryName);
        }

        return $subCategoriesNames;
    }
}
