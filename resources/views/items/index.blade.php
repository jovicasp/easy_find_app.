@extends('layout0')

@section('content')
    <div id="wrapper">
        <div id="page" class="container">
            <div id="content">
                    <a href="{{ route('items.create') }}" class="button">ADD new item</a>
                    <p></p>
                    @forelse($items as $item)
                        <div class="content">
                            <div class="title">
                                <h3><a href="{{$item->path()}}">{{ $item->name }}</a></h3>
                                <h5><b>{{ 'Category: ' }}
                                        <a href="{{ route('categories.show', $item->categories->id) }}">
                                                <i>{{ $item->categories->name }}</i></a></b>
                                </h5>
                                <div style=" display: block; margin-left: 800px; margin-top: -90px;">
                                    <a href="{{ route('items.edit', $item) }}" class="button button-green">Edit</a>
                                </div>

                                <form action="{{ route('items.destroy', $item)  }}" method="post">
                                    <div style=" display: block; margin-left: 900px; margin-top: -90px;">
                                        <button type="submit" value="send" class="button button-red">Delete</button>
                                    </div>
                                    @method('DELETE')
                                    @csrf
                                </form>
                                <div><p>{!! $item->description !!}</p></div>
                                <img src="{{ '/storage/images/' . $item->picture }}" alt="home_item" height="80"
                                     width="80"
                                     style=" display: block; margin-left: auto; margin-right: auto;"/>
                            </div>
                        </div>
                    @empty
                        <p>No relevant items yet.</p>
                    @endforelse
            </div>
        </div>
    </div>
@endsection