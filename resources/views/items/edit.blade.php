@extends('layout0')

@section('head')
    <link href="/css/main.css" rel="stylesheet"/>
@endsection
@section('content')
    <div id="wrapper">
        <div id="page" class="container">
            <div id="content">
                <div class="title">
                    <div class="bg-contact2" style="background-image:url('/images/bg-01.jpg');">
                        <div class="container-contact2">
                            <div class="wrap-contact2">
                                <form class="contact2-form validate-form"
                                      action="{{ route('items.update', $item) }}"
                                      method="POST" enctype="multipart/form-data">
                                    @csrf
                                    @method('PUT')
                                    <span class="contact2-form-title">
                                        EDIT ITEM
                                    </span>
                                    {{--                                    Item name--}}
                                    <div class="wrap-input2 validate-input"
                                         data-validate="Name is required">
                                        <label for="item_name">Name</label>
                                        <input id="item_name"
                                               class="input2"
                                               type="text"
                                               name="item_name"
                                               value="{{ old('item_name', $item->name) }}">
                                        @error('item_name')
                                        <p class="is-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                    {{--                                    Category--}}
                                    <div class="wrap-input2 validate-input" data-validate="Category is required">
                                        <label for="categories">Category</label>
                                        <div></div>
                                        <select id="categories"
                                                class="@error('categories') is-danger @enderror"
                                                name="categories">
{{--                                            <option value="">Choose one</option>--}}
                                            @foreach($categories as $category)
                                                <option value="{{ $category->id }}"
                                                        @if (old("categories", $item->category_id) == $category->id) selected @endif>
                                                    {{ $category->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                        @error('categories')
                                        <p class="is-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                    {{--                                    Item Picture--}}
                                    <div class="wrap-input2 validate-input" data-validate="Picture is required">
                                        <label for="picture">Picture</label>
                                        <div></div>
                                        {{--Previously uploaded picture--}}
                                        <span id="edit-picture"
                                              class="input2">
                                            @if ($item->picture)
                                                <img src="{{ '/storage/images/' . $item->picture }}"
                                                     alt="home_item"
                                                     height="60" width="60"
                                                     style=" display: block; margin-left: auto; margin-right: auto;"/>
                                            @endif
                                        </span>
                                        @error('picture')
                                        <p class="is-danger">{{ $message }}</p>
                                        @enderror
                                        <input id="picture"
                                               class="@error('picture') is-danger @enderror"
                                               type="file"
                                               name="picture"
                                               value="{{ old('picture') }}">
                                        @if ($item->picture)
                                            <h5>Change item's picture</h5>
                                        @else
                                            <h5>Upload item's picture</h5>
                                        @endif
                                    </div>
                                    {{--                                    Item Description--}}
                                    <div class="wrap-input2 validate-input" data-validate="Description is required">
                                        <label for="description">Short description</label>
                                        <div></div>
                                        <textarea id="description"
                                                  class="input2"
                                                  name="description">{{ old('description', $item->description) }}
                                        </textarea>
                                        @error('description')
                                        <p class="is-danger">{{ $message }}</p>
                                        @enderror
                                    </div>

                                    <div class="container-contact2-form-btn">
                                        <div class="wrap-contact2-form-btn">
                                            <div class="contact2-form-bgbtn"></div>
                                            <button class="contact2-form-btn" type="submit" value="Submit">
                                                UPDATE
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection