@extends('layout0')

@section('content')
    <div id="wrapper">
        <div id="page" class="container">
            <div id="content">
                <div class="title">
                    <h2>{{ $item->name }}</h2>
                    <h5><b>{{ 'Category: ' }}
                            <a href="{{ route('categories.show', $item->categories->id) }}">
                                <i>{{ $item->categories->name }}</i></a></b>
                    </h5>
                    <div><img src="{{ '/storage/images/' . $item->picture }}" alt="home_item" height="300" width="300"
                              style=" display: block; margin-left: auto; margin-right: auto;"/></div>

                    {{ $item->description }}
                    <div style=" display: block; margin-left: 800px; margin-top: -90px;">
                        <a href="{{ route('items.edit', $item) }}" class="button button-green">Edit</a>
                    </div>

                    <form action="{{ route('items.destroy', $item)  }}" method="post">
                        <div style=" display: block; margin-left: 900px; margin-top: -90px;">
                            <button type="submit" value="send" class="button button-red">Delete</button>
                        </div>
                        @method('DELETE')
                        @csrf
                    </form>
                </div>
            </div>
        </div>

@endsection