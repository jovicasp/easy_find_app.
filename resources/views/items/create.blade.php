@extends('layout0')

@section('head')
    <link href="/css/main.css" rel="stylesheet"/>
@endsection

@section('content')
    <div id="wrapper">
        <div id="page" class="container">
            <div id="content">
                <div class="title">
                    <div class="bg-contact2" style="background-image:url('/images/bg-01.jpg');">
                        <div class="container-contact2">
                            <div class="wrap-contact2">
                                <form class="contact2-form validate-form"
                                      action="{{ route('items.store') }}"
                                      method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <span class="contact2-form-title">
                                        CREATE ITEM
                                    </span>
                                    {{--                                    Item name--}}
                                    <div class="wrap-input2 validate-input" data-validate="Name is required">
                                        <label for="item_name">Item Name</label>
                                        <input id="item_name"
                                               class="input2"
                                               type="text"
                                               name="item_name"
                                               value="{{ old('item_name') }}">
                                        @error('item_name')
                                        <p class="is-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                    {{--                                    Category--}}
                                    <div class="wrap-input2 validate-input" data-validate="Category is required">
                                        <label for="categories">Category is mandatory! Choose one or
                                            <a href="{{ route('categories.create') }}" ><b><i>create one.</i></b></a></label>
                                        <div></div>
                                        <select id="categories"
                                                class="@error('categories') is-danger @enderror"
                                                name="categories">
                                            <option selected value="chooseone">Choose one</option>
                                            @foreach($categories as $category)
                                                <option value="{{ $category->id }}"
                                                        @if($category->id == old('categories')) selected @endif>{{ $category->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                        @error('categories')
                                        <p class="is-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                    {{--                                    Item Picture--}}
                                    <div class="wrap-input2 validate-input" data-validate="Picture is required">
                                        <label for="picture">Picture</label>
                                        <input id="picture"
                                               class="@error('picture') is-danger @enderror"
                                               type="file"
                                               name="picture"
                                               value="{{ old('picture') }}">
                                        @error('picture')
                                        <p class="is-danger">{{ $message }}</p>
                                        @enderror
                                        <div></div>
                                    </div>
                                    {{--                                    Item Description--}}
                                    <div class="wrap-input2 validate-input" data-validate="Description is required">
                                        <label for="description">Short description</label>
                                        <textarea id="description"
                                                  class="input2"
                                                  name="description">{{ old('description') }}
                                        </textarea>
                                        @error('description')
                                        <p class="is-danger">{{ $message }}</p>
                                        @enderror
                                    </div>

                                    <div class="container-contact2-form-btn">
                                        <div class="wrap-contact2-form-btn">
                                            <div class="contact2-form-bgbtn"></div>
                                            <button class="contact2-form-btn" type="submit" value="Submit">
                                                CREATE
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection