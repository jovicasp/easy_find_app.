@extends('layout0')

@section('head')
    <link href="/css/main.css" rel="stylesheet"/>
@endsection

@section('content')
    <div id="wrapper">
        <div id="page" class="container">
            <div id="content">
                <div class="title">
                    <div class="bg-contact2" style="background-image:url('/images/bg-01.jpg');">
                        <div class="container-contact2">
                            <div class="wrap-contact2">
                                <form class="contact2-form validate-form"
                                      action="{{ route('categories.store') }}"
                                      method="POST">
                                    @csrf
                                    <span class="contact2-form-title">
                                        CREATE NEW CATEGORY/SUBCATEGORY
                                    </span>
                                    {{--                                    Category name--}}
                                    <div class="wrap-input2 validate-input" data-validate="Name is required">
                                        <label for="category-name">Category Name</label>
                                        <input id="category-name"
                                               class="input2"
                                               type="text"
                                               name="category-name"
                                               value="{{ old('category-name') }}">
                                        @error('category-name')
                                        <p class="is-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                    {{--                                    Parent Category--}}
                                    <div class="wrap-input2 validate-input" data-validate="Category is required">
                                        <label for="categories">Parent Category. May be Zero category, without parent one.</label>
                                        <div></div>
                                        <select id="categories"
                                                name="parent-category">
                                            <option selected value="">No parent category</option>
                                            @foreach($categories as $parentCategory)
                                                <option value="{{ $parentCategory->id }}"
                                                        @if($parentCategory->id == old('parent-category')) selected @endif>{{ $parentCategory->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                        @error('parent-category')
                                        <p class="is-danger">{{ $message }}</p>
                                        @enderror
                                    </div>

                                    <div class="container-contact2-form-btn">
                                        <div class="wrap-contact2-form-btn">
                                            <div class="contact2-form-bgbtn"></div>
                                            <button class="contact2-form-btn" type="submit" value="Submit">
                                                CREATE
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection