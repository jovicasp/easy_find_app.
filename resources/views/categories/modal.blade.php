<div class="modal fade" id="confirm">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Delete Confirmation</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <p>Are you sure you, want to delete this category ?  <br><span id="warning_span"></span></p>
            </div>
            <div class="modal-body">
                <p>If You want to see what will be deleted click <a href="" id="showLink" target="_blank"> here.</a></p>
            </div>
            <div class="modal-footer">
                <form action="" method="post" class="form-delete" id="deleteForm">
                    <button type="submit"   class="button button-red1" id="delete-btn" style="margin-top: 0.5em;">Delete</button>
                    @method('DELETE')
                    @csrf
                </form>
                <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>