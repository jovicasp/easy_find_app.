<li><a href="{{$child_category->path()}}">{{ $child_category->name }}</a></li>
<div style=" display: block; margin-left: 300px; margin-top: -80px;">
    <a href="{{ route('categories.edit', $child_category) }}" class="button button-green1">Edit</a>
</div>
@include('categories.modal')
    <div style=" display: block; margin-left: 400px; margin-top: -85px;">
        <button type="button"
                value="send"
                data-toggle="modal"
                data-target="#confirm"
                data-category-id = "{{ $child_category->id }}"
                class="button button-red"
                @if ((($child_category->items)->isNotEmpty()) || (($child_category->categories)->isNotEmpty()))
                data-category-state = "{{ 'This category contains item-s or/and subcategory-ies.
                                                If You say Yes,' }} . <b><i>{!!  'You will delete its all items, subcategories and theirs items!' !!}</i></b>"
                @else
                data-category-state = "{{ 'This category does not contain any item-s or/and subcategory-ies.' }}"
                @endif
        >Delete
        </button>
    </div>
@if ($child_category->categories)
    <ul>
        @foreach ($child_category->categories as $childCategory)
            @include('categories.child_category', ['child_category' => $childCategory])
        @endforeach
    </ul>
@endif