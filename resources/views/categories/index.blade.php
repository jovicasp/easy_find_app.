@extends('layout0')

@section('content')
    <div id="wrapper">
        <div id="page" class="container">
            <div id="content">
                <a href="{{ route('categories.create') }}" class="button">ADD new category</a>
                <p></p>
                @forelse($categories as $category)
                    <div class="content dialog">
                        <div class="title">
                            <h3><a href="{{$category->path()}}">{{ $category->name }}</a></h3>
                            <div style=" display: block; margin-left: 500px; margin-top: -90px;">
                                <a href="{{ route('categories.edit', $category) }}" class="button button-green">Edit</a>
                            </div>
                            @include('categories.modal')
                            <div style=" display: block; margin-left: 600px; margin-top: -90px;">
                                <button type="button"
                                        value="send"
                                        data-toggle="modal"
                                        data-target="#confirm"
                                        data-category-id = "{{ $category->id }}"
                                        class="button button-red"
                                        @if ((($category->items)->isNotEmpty()) || (($category->categories)->isNotEmpty()))
                                                data-category-state = "{{ 'This category contains item-s or/and subcategory-ies.
                                                If You say Yes,' }} . <b><i>{!!  'You will delete its all items, subcategories and theirs items!' !!}</i></b>"
                                        @else
                                                data-category-state = "{{ 'This category does not contain any item-s or/and subcategory-ies.' }}"
                                        @endif
                                >Delete
                                </button>
                            </div>
                            <ul>
                                @foreach ($category->childrenCategories as $childCategory)
                                    @include('categories.child_category', ['child_category' => $childCategory])
                                @endforeach
                            </ul>
                        </div>
                    </div>
                @empty
                    <p>No relevant categories yet.</p>
                @endforelse
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $( document ).ready(function() {
         $('.button-red').click(function(e){
            e.preventDefault();
             $('#warning_span').html($(this).attr('data-category-state'));
             var id = $(this).data('category-id');
             var url_destroy = '{{  route('categories.destroy', ":id") }}';
             var url_show = '{{  route('categories.show', ":id") }}';
             url_destroy = url_destroy.replace(':id', id);
             url_show = url_show.replace(':id', id);
             $("#deleteForm").attr('action', url_destroy);
             $("#showLink").attr('href', url_show);
         })
        });
    </script>
@endsection