
    <div id="wrapper">
        <div id="page" class="container">
            <div id="content">
                <h2>Found categories:</h2>
                <ul>
                    @foreach ($zerocategories as $zerocategory)
                        <h3><a href="{{$zerocategory->path()}}">{{ '   ---->' }}{{ $zerocategory->name }}</a></h3>
                    @endforeach
                </ul>
                <h2>Found subcategories:</h2>
                <ul>
                    @foreach ($subcategories as $subcategory)
                        <h3><a href="{{$subcategory->path()}}">{{ '   ---->' }}{{ $subcategory->name }}</a></h3>
                    @endforeach
                </ul>
                <h2>Found items:</h2>
                <ul>
                    @foreach ($items as $item)
                        <span style="font-weight: bold;"><li><h3><a href="{{$item->path()}}">{{ $item->name }}</a></h3></li></span>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>