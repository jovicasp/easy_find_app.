@extends('layout0')

@section('content')
    <div id="wrapper">
        <div id="page" class="container">
            <div id="content">
                <div class="title">
                    <h1>{{ "Category : " }}<b>{{ $category->name }}</b></h1>
                    <p></p>
                    <h1>{{ "Root items : " }}
                        @if (($category->items)->isEmpty())
                            <span style="font-size:18px"><i>{{ "This category does not contain any root items." }}</i></span>
                        @endif
                        @foreach($category->items as $item)
                            <h5><a href="{{$item->path()}}">{{ $item->name }} {{ ', ' }}</a></h5>
                        @endforeach
                    </h1>
                    <p></p>
                    <h1>{{ "SubCategory/ies : " }}
                        @if (($category->categories)->isEmpty())
                            <span style="font-size:18px"><i>{{ "This category does not contain any subcategories." }}</i></span>
                        @endif
                        @foreach($category->categories as $subCategory)
                            <h3><a href="{{$subCategory->path()}}">{{ '   ---->' }}{{ $subCategory->name }}</a></h3>
                            @foreach($subCategory->items as $item)
                                <h5><a href="{{$item->path()}}">{{ $item->name }} {{ ', ' }}</a></h5>
                            @endforeach
                            @if($subCategory->categories)
                                @foreach($subCategory->getAllChildrenCategories() as $subCategory1)
                                    <a href="{{$subCategory1->path()}}">{{ '   ---->' }}{{ $subCategory1->name }}</a>
                                    @foreach($subCategory1->items as $item)
                                        <h5><a href="{{$item->path()}}">{{ $item->name }} {{ ', ' }}</a></h5>
                                    @endforeach
                                @endforeach
                            @endif
                        @endforeach
                    </h1>
                </div>
            </div>
        </div>
@endsection