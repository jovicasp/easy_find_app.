@extends('layout0')

@section('head')
    <link href="/css/search.css" rel="stylesheet"/>
@endsection

@section('header-featured')
    <div id="header-featured">

            <div id="banner" class="container">
                <h2>Welcome to Easy Find page</h2>
                <p>This is <strong>Easy Find</strong>, project for searching for any consumer goods. </br>Please login to start Your search.
                </p>
                    <form class="form-wrapper" action="{{ route('categories.search') }}" method="GET">
                        <input type="text" id="search" placeholder="Search for..." name="input" required>
                        {{--<button type="submit" value="go" id="submit">FIND ING</button>--}}
                    </form>
            </div>
            <div id="results" style="background-color: gray(); padding: 15px;"></div>
    </div>
@endsection

@section('scripts')
    <script>
        $("#search").keyup(function(event) {
            event.preventDefault();
            $.ajax({
                method: "GET",
                url: "/search?input=" + $("#search").val(),
                // data: { name: "John", location: "Boston" }
            })
            .done(function(result) {
                $("#results").html(result);
            });
        });
    </script>
@endsection