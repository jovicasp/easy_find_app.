# Easy Find App  
1. Go to local folder, where project will be placed, and Clone the repository (SSH): Run:
- `git clone git@gitlab.com:jovicasp/easy_find_app..git`  
2. Stop and remove all existing docker containers (and docker images), if You have any:
- `docker ps -a`
- `docker image ls`
- `docker stop $(docker ps -a -q)`
- `docker rm $(docker ps -a -q)`
- `docker image prune -a`
- `sudo service docker restart`                 !!!  
2.1 Build and start all containers:  
- `docker-compose build`
- `docker-compose up -d`
3. Install all packages and dependencies:
- `composer install`  
4. Copy file .env.example as .env on root and set it up:
- DB_CONNECTION=mysql
- DB_HOST=db
- DB_PORT=3306
- DB_DATABASE=laravel_easy_find
- DB_USERNAME=laraveluser   (it could be root for start, but good practice is to change it)
- DB_PASSWORD=SqlLaravel2020
5. Generate application encryption key to secure session and encrypt data:
- `docker-compose exec app php artisan key:generate`  
6. Cache the .env settings to speed up app loading:  
- `docker-compose exec app php artisan config:cache`
### And Voila:[localhost](http://localhost)  
7. MYSQL - create DB and DB_user with granted privileges - all data are from docker-compose.yml file(line 47, 48) and .env file(line 12,13,14). 
- `docker-compose exec db bash`
- root@4d40397e8c25:/# `mysql -u root -p`
- (insert password for your root mysql user)
- mysql> `CREATE DATABASE laravel_easy_find;` 
- mysql> `show databases;`
- mysql> `GRANT ALL ON laravel_easy_find.* TO 'laraveluser'@'%' IDENTIFIED BY 'SqlLaravel2020';`
- mysql> `FLUSH PRIVILEGES;`
- mysql> `EXIT;`
- `exit` <br/>
(In order not have to enter bash-shell-terminal of the app container, EVERY time, when we need php artisan command, we made file - 'command' into app's root directory. So, from now on, we use 'command' along with php artisan like: `./command php artisan...`)  
8. Migrate the data:
    - `./command php artisan migrate`
9. In PhpStorm set the DB - Database->DataSource->Mysql (MariaDB)-> insert data from .env file and test connection.  Import data from root file dump.sql. Right click and `Run dump.sql...`, select DB - laravel_easy_find, OK.  If you, in any case, change the DB name, You will need to change it adequately in file dump.sql(line 22, 23).  

If You want some pictures: Directory - 'images' is located in root directory, and it is filled with some items photos. You should move/copy it in the filesystem, under directory: 'storage/app/public'. Ensure having 755 permissions for that sub-folder - 'images'. 
In case You do not see the pictures, You will need to do following:

- remove-delete storage folder or link from 'public' directory: `rm storage`  
- go to Laravel app's root directory and run:`./command php artisan storage:link`
That way, we make symbolic link between 'storage/app/public' and 'public/storage'. 
This needs to be done EVERY time when Laravel project is moved/copied/deployed!!<br/>
CREDENTIALS:UN-admin@mail.com, PSW-admin123, UN-user@mail.com, PSW-12345678. 
